"""poll wrapper class"""

import datetime

class PleromaPoll:
    """
    wrapper class for handling polls
    """
    def __init__(self,
            options: list[str],
            expires_in: datetime.timedelta,
            multiple: bool = None,
            hide_totals: bool = None):
        self._options = options
        self._expires_in = expires_in.total_seconds()
        self._multiple = multiple
        self._hide_totals = hide_totals

    @classmethod
    def from_response(cls, data: dict):
        """
        alternative constructor to create object from response of a server

        as the app is used to only view scheduled posts, this constructor doesn't
        retain information about vote counts.
        """
        return cls(expires_in=datetime.datetime.fromisoformat(data["expires_in"]),
                multiple=data["multiple"],
                options=[i["title"] for i in data["options"]])

    @property
    def options(self):
        """
        a list of strings, which are options for poll
        """
        return self._options

    @property
    def expires_in(self):
        """
        timedelta, which represents time in which poll will expire
        """
        return self._expires_in

    @property
    def multiple(self):
        """
        boolean flag, True if poll allows multiple choices
        """
        return self._multiple

    @multiple.setter
    def multiple(self, flag: bool):
        self._multiple = flag

    @property
    def hide_totals(self):
        """
        boolean flag, True if counts are hidden until poll ends
        """
        return self._hide_totals

    @hide_totals.setter
    def hide_totals(self, flag: bool):
        self._hide_totals = flag

    def __repr__(self):
        return f"PleromaPoll(options={self._options},\
expires_in={self._expires_in},\
multiple={self._multiple},\
hide_totals={self._hide_totals})"
