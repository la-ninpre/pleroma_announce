"""classes for status objects"""

import datetime as dt

from poll import PleromaPoll

class PleromaScheduledStatus:
    """
    scheduled status type
    """
    def __init__(self,
            s_id: str,
            scheduled_at: dt.datetime,
            params: dict,
            media_attachments: list):
        self._s_id = s_id
        self._scheduled_at = scheduled_at
        self._params = params
        self._media_attachments = media_attachments

    @classmethod
    def from_response(cls, data: dict):
        """
        alternative constructor to create object from response of a server
        """
        try:
            media_attachments = data["media_attachments"]
        except KeyError:
            media_attachments = None
        return cls(s_id=data["id"],
                scheduled_at=dt.datetime.fromisoformat(data["scheduled_at"].replace("Z","")),
                params=PleromaScheduledStatusParams.from_response(data["params"]),
                media_attachments=media_attachments)

    @property
    def s_id(self):
        """status id"""
        return self._s_id

    @property
    def scheduled_at(self):
        """time to which post is scheduled"""
        return self._scheduled_at

    @property
    def params(self):
        """params object"""
        return self.params

    @property
    def media_attachments(self):
        """attachments"""
        return self.media_attachments

    def __repr__(self):
        return f"PleromaScheduledStatus(s_id={self._s_id},\
scheduled_at={repr(self._scheduled_at)},\
params={repr(self._params)},\
media_attachments={self._media_attachments})"

    def __str__(self):
        # TODO: add poll
        return f"[#{self._s_id}] - {self._params.text}"



class PleromaScheduledStatusParams:
    """
    wrapper class for params of scheduled status

    note: only requided attributes implemented here, because i won't ever need
    other things just for announcing. i don't want to implement full pleroma
    api and spend here another eternity.
    """
    def __init__(self,
            text: str,
            visibility: str,
            poll: PleromaPoll = None):
        self._poll = poll
        self._text = text
        self._visibility = visibility

    @classmethod
    def from_response(cls, data: dict):
        """
        alternative constructor to make object from response of a server
        """
        if data["poll"] is not None:
            poll = PleromaPoll.from_response(data["poll"])
        else:
            poll = None
        return cls(text=data["text"],
                visibility=data["visibility"],
                poll=poll)

    @property
    def poll(self):
        """poll object if any"""
        return self._poll

    @property
    def text(self):
        """text of a status"""
        return self._text

    @property
    def visibility(self):
        """
        visibility of a status
        """
        return self._visibility

    def __repr__(self):
        return f"PleromaScheduledStatusParams(text={self._text},\
visibility={self._visibility},\
poll={repr(self._poll)})"
