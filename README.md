# pleroma announce

this is a small quick and dirty tool that intended to be used to post
announcements and polls onto bot on pleroma.

## prerequisites

- python 3

- pip

- curl (optional, if you can replace it with other tool)

- registered pleroma app and authentication token (see next section)

## pleroma app registration

1. create pleroma user and mark it as bot.

2. create an application for pleroma to get `client_id` and `client_secret`.
refer to [pleroma api specification][1] and [mastodon api specification][2].
this tool requires `write:statuses` and `read:statuses` scopes, so don't forget
to specify them, otherwise it will fail.

  ```terminal
  curl -X POST \
      -F 'client_name={YOUR_APP_NAME}' \
      -F 'redirect_uris=urn:ietf:wg:oauth:2.0:oob' \
      -F 'scopes=write:statuses read:statuses' \
      https://yourdomain.tld/api/v1/apps
  ```
  [1]: https://api.pleroma.social/#operation/AppController.create
  [2]: https://docs.joinmastodon.org/client/authorized/

3. with your `client_id` authorize your application with
account you are planning to use for posting.

  in browser enter:
  ```
  https://yourdomain.tld/oauth/authorize?client_id={YOUR_CLIENT_ID}
  &scope=write:statuses+read:statuses
  &redirect_uri=urn:ietf:wg:oauth:2.0:oob
  &response_type=code
  ```

  after entering credentials, you will receive access code which
  will be used in the next step.

4. with obtained access code you have to aquire authentication token for
the application.

  ```terminal
  curl -X POST \
      -F 'client_id={YOUR_CLIENT_ID}' \
      -F 'client_secret={YOUR_CLIENT_SECRET}' \
      -F 'redirect_uri=urn:ietf:wg:oauth:2.0:oob' \
      -F 'grant_type=authorization_code' \
      -F 'code={YOUR_AUTHERIZATION_CODE}' \
      -F 'scope=write:statuses read:statuses' \
      https://yourdomain.tld/oauth/token
  ```

  you will json response with authentication token, now save it to `.env` file,
  see `.env.example`.

## usage

1. git clone the repo

  ```terminal
  $ git clone https://git.aaoth.xyz/pleroma_announce.git
  $ cd pleroma_announce
  ```

2. install required pip packages
(though they are likely to be installed already)

  ```terminal
  $ pip install -r requirements.txt
  ```

3. now you can use the script, see `--help` for options. for example:

  ```terminal
  $ python plann.py post -d 2022-02-14T14:00+00:00 -p opt1 opt2 -e 3 -m test
  ```

  this will schedule a status with text `test` and poll with options `opt1` and
  `opt2` to 14 feb 2022 14:00 UTC.

## todos

- add setup.py

- figure out versions of pip packages

- handle media attachments

- add man page

## contacts

[email me][3] or reach me on [fedi][4] if you have found a bug or
just something weird.

[3]:mailto:aaoth@aaoth.xyz
[4]:https://pleroma.aaoth.xyz/users/la_ninpre

## licence

this program is licensed under an isc licence, see `LICENCE.txt` for details.
